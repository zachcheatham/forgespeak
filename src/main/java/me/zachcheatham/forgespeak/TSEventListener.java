package me.zachcheatham.forgespeak;

import cpw.mods.fml.common.FMLLog;
import de.stefan1200.jts3serverquery.TeamspeakActionListener;
import org.apache.logging.log4j.Level;

import java.util.HashMap;

public class TSEventListener implements TeamspeakActionListener
{
	private final ForgeSpeak instance;
	public TSEventListener(ForgeSpeak instance)
	{
		this.instance = instance;
	}

	@Override
	public void teamspeakActionPerformed(String eventType, HashMap<String, String> eventInfo)
	{
		// If we don't catch, JTS3 will and it stops the listen thread.
		try
		{
			if (eventType.equals("notifytextmessage"))
			{
				ChatMessenger.showChatMessage(eventInfo.get("targetmode").equals("3"), eventInfo.get("invokername"), eventInfo.get("msg"));
			}
			else if (eventType.equals("notifycliententerview"))
			{
				int clientID = Integer.parseInt(eventInfo.get("clid"));
				String nickname = TSHelper.findClientName(instance.getQueryClient(), clientID);
				ChatMessenger.showConnect(nickname);
			}
			else if (eventType.equals("notifyclientleftview"))
			{
				int clientID = Integer.parseInt(eventInfo.get("clid"));
				String nickname = TSHelper.findClientName(instance.getQueryClient(), clientID);
				String leavingMessage = eventInfo.get("reasonmsg");
				ChatMessenger.showDisconnect(nickname, leavingMessage);

				TSHelper.removeCachedName(clientID);
			}
			else if (eventType.equals("notifyclientmoved"))
			{
				int targetChannel = Integer.parseInt(eventInfo.get("ctid"));
				int clientID = Integer.parseInt(eventInfo.get("clid"));
				String nickname = TSHelper.findClientName(instance.getQueryClient(), clientID);

				ChatMessenger.showMove(targetChannel == instance.getListenChannel(),
						Integer.parseInt(eventInfo.get("reasonid")),
						nickname);
			}
			else
			{
				FMLLog.log(ForgeSpeak.NAME, Level.INFO, "Unhandled event: " + eventType);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
