package me.zachcheatham.forgespeak;

import de.stefan1200.jts3serverquery.JTS3ServerQuery;

import java.util.HashMap;

public class TSKeepAlive implements Runnable
{
	private final ForgeSpeak instance;
	public TSKeepAlive(ForgeSpeak instance)
	{
		this.instance = instance;
	}

	@Override
	public void run()
	{
		JTS3ServerQuery query = instance.getQueryClient();

		while (true)
		{
			try
			{
				Thread.sleep(30000);
			}
			catch (InterruptedException e)
			{
				return;
			}

			if (query.isConnected())
			{
				HashMap<String, String> response = query.doCommand("clientupdate");
				if (!response.get("msg").equalsIgnoreCase("ok"))
				{
					ChatMessenger.showServerLost();
					instance.connectToTS();
				}
			}
			else
			{
				instance.connectToTS();
			}
		}
	}
}
