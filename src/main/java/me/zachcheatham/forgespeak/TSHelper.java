package me.zachcheatham.forgespeak;

import cpw.mods.fml.common.FMLLog;
import de.stefan1200.jts3serverquery.JTS3ServerQuery;
import net.minecraft.util.EnumChatFormatting;
import org.apache.logging.log4j.Level;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

public class TSHelper
{
	private static final Map<Integer, String> tsNameCache = new HashMap<Integer, String>();

	public static String findClientName(JTS3ServerQuery query, int clientID)
	{
		String name = tsNameCache.get(clientID);

		if (name == null)
		{
			String command = "clientinfo clid=" + clientID;
			HashMap<String, String> response = query.doCommand(command);

			if (!response.get("msg").equalsIgnoreCase("ok"))
			{
				FMLLog.log(ForgeSpeak.NAME, Level.WARN, "Unable to get clientID of #" + clientID);
				return EnumChatFormatting.ITALIC + "Unknown" + EnumChatFormatting.RESET;
			}

			HashMap<String, String> responseData = parseKeyValues(response.get("response"));
			name = responseData.get("client_nickname");

			putCachedName(clientID, name);
		}

		return name;
	}

	public static void getAllNames(JTS3ServerQuery query)
	{
		clearCache();

		Vector<HashMap<String, String>> clientList = query.getList(JTS3ServerQuery.LISTMODE_CLIENTLIST, "-info");
		if (clientList != null)
		{
			for (HashMap<String, String> clientInfo : clientList)
			{
				int id = Integer.parseInt(clientInfo.get("clid"));
				String nick = clientInfo.get("client_nickname");
				putCachedName(id, nick);
			}
		}
	}

	public static void putCachedName(int clientID, String name)
	{
		tsNameCache.put(clientID, name);
	}

	public static void removeCachedName(int clientID)
	{
		tsNameCache.remove(clientID);
	}

	private static HashMap<String, String> parseKeyValues(String data)
	{
		HashMap<String, String> mappedData = new HashMap<String, String>();

		for (String pair : data.split(" "))
		{
			String[] keyValues = pair.split("=");
			if (keyValues.length > 1)
				mappedData.put(keyValues[0], keyValues[1]);
		}

		return mappedData;
	}

	public static void clearCache()
	{
		tsNameCache.clear();
	}
}
