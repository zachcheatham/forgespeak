package me.zachcheatham.forgespeak;

import cpw.mods.fml.common.FMLLog;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartedEvent;
import cpw.mods.fml.common.event.FMLServerStoppingEvent;
import de.stefan1200.jts3serverquery.JTS3ServerQuery;
import net.minecraftforge.common.config.Configuration;
import org.apache.logging.log4j.Level;

@Mod(modid = ForgeSpeak.MOD_ID, name=ForgeSpeak.NAME, version = ForgeSpeak.VERSION, acceptableRemoteVersions = "*")
public class ForgeSpeak
{
	public static final String MOD_ID = "forgespeak";
	public static final String VERSION = "1.0.1";
	public static final String NAME = "ForgeSpeak";

	private JTS3ServerQuery tsQueryClient;
	private TSEventListener tsEventListener;
	private Thread keepAliveThread;

	private String serverAddress;
	private int serverVirtualPort;
	private int serverQueryPort;

	private String queryUsername;
	private String queryPassword;

	private int channelID;
	private String channelPassword;

	@EventHandler
	public void preInit(FMLPreInitializationEvent event)
	{
		Configuration config = new Configuration(event.getSuggestedConfigurationFile());
		config.load();

		serverAddress = config.get("server-info", "address", "").getString();
		serverVirtualPort = config.get("server-info", "virtual-port", 0).getInt();
		serverQueryPort = config.get("server-info", "query-port", 0).getInt();
		queryUsername = config.get("server-info", "query-user", "").getString();
		queryPassword = config.get("server-info", "query-password", "").getString();
		channelID = config.get("server-info", "channel-id", 0).getInt();
		channelPassword = config.get("server-info", "channel-password", "").getString();

		config.save();
	}

	@EventHandler
	public void serverStarted(FMLServerStartedEvent event)
	{
		tsQueryClient = new JTS3ServerQuery();
		tsQueryClient.DEBUG = true;

		connectToTS();

		// Start Keep Alive
		keepAliveThread = new Thread(new TSKeepAlive(this));
		keepAliveThread.start();
	}

	@EventHandler
	public void serverStopping(FMLServerStoppingEvent event)
	{
		keepAliveThread.interrupt();
		tsQueryClient.closeTS3Connection();
		FMLLog.log(NAME, Level.INFO, "Disconnected.");
	}

	public void connectToTS()
	{
		FMLLog.log(NAME, Level.INFO, "Connecting to TeamSpeak...");
		if (!tsQueryClient.connectTS3Query(serverAddress, serverQueryPort))
		{
			tsQueryClient.closeTS3Connection();
			FMLLog.log(NAME, Level.ERROR, "Failed to connect!");
			return;
		}

		FMLLog.log(NAME, Level.INFO, "Logging in...");
		tsQueryClient.loginTS3(queryUsername, queryPassword);

		tsEventListener = new TSEventListener(this);
		tsQueryClient.setTeamspeakActionListener(tsEventListener);

		if (!tsQueryClient.selectVirtualServer(serverVirtualPort, true))
		{
			logTSError();
			return;
		}

		if (!tsQueryClient.moveClient(tsQueryClient.getCurrentQueryClientID(), channelID, channelPassword))
		{
			logTSError();
			return;
		}

		if (!tsQueryClient.addEventNotify(JTS3ServerQuery.EVENT_MODE_CHANNEL, channelID))
		{
			logTSError();
			return;
		}

		if (!tsQueryClient.addEventNotify(JTS3ServerQuery.EVENT_MODE_TEXTSERVER, 0))
		{
			logTSError();
			return;
		}

		if (!tsQueryClient.addEventNotify(JTS3ServerQuery.EVENT_MODE_TEXTCHANNEL, channelID))
		{
			logTSError();
			return;
		}

		tsQueryClient.setDisplayName("ForgeSpeak");

		FMLLog.log(NAME, Level.INFO, "Connected.");
		ChatMessenger.showServerConnected();

		TSHelper.getAllNames(tsQueryClient);
	}

	private void logTSError()
	{
		String message = tsQueryClient.getLastError();
		if (message != null)
		{
			FMLLog.log(NAME, Level.ERROR, message);
		}
	}

	public JTS3ServerQuery getQueryClient()
	{
		return tsQueryClient;
	}

	public int getListenChannel()
	{
		return channelID;
	}
}
